#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <vector>
#include <cstdlib>
#include <string>

#include "raw_data_set.h"

class Timer {
public:
    std::chrono::time_point<std::chrono::system_clock> start, end;

	Timer(){}
	~Timer(){}

	void Start() {
		start = std::chrono::system_clock::now();
	}

	double Stop() {
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed = end-start;
		std::cout << elapsed.count() << " seconds" << std::endl;
		return elapsed.count();
	}
};

// Read data into memory from disk
int count = 0;
void ReadFile(std::string s) {
	std::filesystem::path p{s};
	size_t file_size = std::filesystem::file_size(p);
	char * data = (char *)malloc(file_size);
	std::ifstream input_file(p);
	input_file.read(data, file_size);

	if (data[rand()%256]==0x1) {
		count++;
	}

	free(data);
}

void ReadDataSet(std::string s) {
	std::filesystem::path p{s};
	for (const auto & entry : std::filesystem::directory_iterator(p)) {
		ReadFile(entry.path());
	}
}

int main(int argc, char ** argv) {

	Timer t;

	std::string v1_data_dir(argv[1]);
	std::string v2_data_file(argv[2]);

	std::cout << v1_data_dir << std::endl;
	std::cout << v2_data_file << std::endl;


	int N=5;
	std::vector<double> v1_perf(N);
	std::vector<double> v2_perf(N);

	RawData * r;
	for (int i=0;i<N;i++){
		r = new RawData();
		t.Start();
		ReadV2Dataset(v2_data_file, r);
		v2_perf[i] = t.Stop();

		if (i==9) {
			r->PrintSomeValues(10);
		}

		delete r;
	}

	for (int i=0;i<N;i++){
		r = new RawData();
		t.Start();
		ReadV1Dataset(v1_data_dir, r);
		v1_perf[i] = t.Stop();

		if (i==9) {
			r->PrintSomeValues(10);
		}

		delete r;
	}

	std::cout << "========================================" << std::endl;
	std::cout << "Final results:" << std::endl;
	std::cout << "DICOM-CT-PD" << std::endl;
	for (int i =0;i<N;i++) {
		std::cout << "  " << v1_perf[i] << std::endl;
	}

	std::cout << "SF-CT-PD" << std::endl;
	for (int i =0;i<N;i++) {
		std::cout << "  " << v2_perf[i] << std::endl;
	}
	std::cout << "========================================" << std::endl;
	return 0;
}
