# cxx-reader-dcmtk

This reader evaluates performance of the original DICOM-CT-PD format against the single-file version we are proposing using a C++ reader implemented using the DCMTK library.  We use DCMTK since it is highly optimized and (in our understanding) the de facto C++ DICOM library.

## Performance results, implementation 1:

In this implementation, we optimize away one full copy of the dataset for SF-CT-PD, which results in ~1sec of performance improvement.

The single-file approach lends itself more naturally to this type optimization for the current implementation.  However, it is possible to implement a version of the RawData class that would prevent these copies, it would be signficantly "clumsier" to do so since we would need to store pointers to the incoming projections.  That being said, a "projection" and "projectionstack" style-approach would reduce this "clumsiness."

These tests read the entire projection dataset (i.e. no random/read writes)

```
> ./read-test ~/Data/TCIA/TCIA-LDCT-PD/C001/ ~/Data/TCIA/TCIA-LDCT-PD-v2/C001.dcm
Final results:
DICOM-CT-PD
  2.61839
  2.65985
  2.67361
  2.67806
  2.67262
SF-CT-PD
  1.83875
  1.91469
  1.62456
  1.6039
  1.72734
```

## Performance results, implementation 2

This implementation removes the optimization above, and uses the same `SetProjection` interface to the RawData class as the DICOM-CT-PD reader.  We see under these circumstances, performance is equivalent.

Although there is not a performance gain (or rather, the gain can clearly be seen to be due to the additional memcpy), the simplicity with which we CAN optimize SF-CT-PD reflects my general experience with usability of the single-file version compared to the many-file version.

```
> ./read-test ~/Data/TCIA/TCIA-LDCT-PD/C001/ ~/Data/TCIA/TCIA-LDCT-PD-v2/C001.dcm
Final results:
DICOM-CT-PD
  2.68249
  2.69184
  2.68728
  2.69536
  2.68829
SF-CT-PD
  2.67917
  2.60468
  2.47671
  2.57028
  2.60591
```

## Takeaway from the exercise

In no particular order:

1. definitely easier to implement the reader for SF-CT-PD (i.e. no file handling, directories, etc. Big win for easier coding, and ultimately more maintainable readers due to less code)
2. SF-CT-PD lends itself somewhat more naturally to reducing memory copies, although we could implement a version for the other one that could accomplish the same thing, so this is kind of a wash.  I can explain more if needed.
3. I think the big difference between MATLAB/PyDICOM and my DCMTK-based readers are that DCMTK is optimized to not have to read/parse the whole dicom file to extract just the fields your interested in and the pixel data (note: I'm NOT parsing anything other than rows, columns, and detector_rows, detector_columns).  That lessens the otherwise significant overhead of parsing the whole header at the time of file read (which I'd guess is why v1/v2 ratios for MATLAB and python are so much more than DCMTK).
4. For a optimized reader that doesn't take the naive approach of parsing the entire DICOM dataset for every file, there is little read performance difference between DICOM-CT-PD and our SF-CT-PD proposal.  Decisions on which file to use can safely come down to the required application/use case, and user preference.  FreeCT will support both formats and provide file conversion tools for users to convert to whichever format better supports their needs.
5. It's pretty safe to conclude that in both cases, the cost of the reconstruction, or whatever processing we're going to do with the raw data, will likely be significantly more than the cost of reading either file format.
6. The two things that occupy the most compute for this exercise are (1) memory copies and (2) Applying rescale_slope/intercept and promotion from int16 to float

## Conclusion

All this being said, most folks are probably not interested high-performance reconstruction (or aren't building/maintaining a software package like FreeCT) are likely to use MATLAB or Python. In those matlab or python, without additional optimization on the part of the user or library maintainers, SF-CT-PD does present signficant possible performance improvements.