// NOTE: An actual C++ reader should probably implement similar
// "architecture" to our FreeCT Go readers for a fully
// apples-to-apples comparison.  I.e. A Projection class, a Geometry
// class, and a ProjectionStack class.  That being said, this is not
// intended to be any sort of official reader (yet).
//
// Also, this version is more C-like than C++.  Using STL stuff
// would make it at the very least more memory-safe, and likely
// also have some low-level optimizations that may not be present
// here.

#include <set>

#include "dcmtk/config/osconfig.h"
#include "dcmtk/dcmdata/dcjson.h"
#include "dcmtk/dcmdata/dctk.h"
#include "dcmtk/dcmimgle/dcmimage.h"

class RawData {
private:
	size_t m_rows;
	size_t m_chan;
	size_t m_proj;
	float * m_projections;
	bool m_initialized = false;

public:

	~RawData() {
		if (m_initialized) {
			free(m_projections);
		}
	}

	void Allocate(size_t rows, size_t chan, size_t proj) {
		m_rows = rows;
		m_chan = chan;
		m_proj = proj;
		m_projections = (float*)malloc(m_rows * m_chan * m_proj * sizeof(float));
		m_initialized = true;

		std::cout << m_rows << std::endl;
		std::cout << m_chan << std::endl;
		std::cout << m_proj << std::endl;
	}

	// Note: there are almost certainly more optimized ways to
	// implement this doing byte-level tricks and reducing memory
	// copies to their absolute minimum, however, most folks aren't
	// interested in going that deep, and will likely be working in
	// higher-level languages (or don't want to spend 6mos optimizing
	// a super duper low-level reader that will likely end up being
	// super brittle.)  We are one of those groups, so this seems like
	// a reasonable interface balancing simplicity, readability, and
	// performance.  We will do our best to reduce copies where
	// obvious, but aren't going to go crazy.
	void SetProjection(size_t proj_idx, float * incoming) {
		size_t offset = proj_idx * m_rows * m_chan;
		memcpy(&m_projections[offset], incoming, m_rows * m_chan * sizeof(float));
	}

	void Set(size_t rows, size_t chan, size_t proj, float * incoming) {
		m_rows = rows;
		m_chan = chan;
		m_proj = proj;
		m_projections = incoming;
		m_initialized = true;
	}

	void PrintSomeValues(int N) {
		for (int i=0;i<N; i++){
			for (int j=0;j<10;j++){
				size_t idx = m_rows * m_chan * i + j;
				std::cout << m_projections[idx] << " ";
			}
			std::cout << std::endl;
		}
	}

	bool IsInitialized() {
		return m_initialized;
	}

};

void ReadV1Dataset(std::string dirpath, RawData * raw) {
	// First, we have to do our directory handling.  This is a REQUIREMENT
	// with the DICOM-CT-PD format when using multiple files.
	//
	// Here, we copy all filenames to a set which gives us the files sorted in the
	// correct order.
	std::filesystem::path p{dirpath};

	std::set<std::filesystem::path> sorted_by_name;

	for (const auto & entry : std::filesystem::directory_iterator(p)) {
		sorted_by_name.insert(entry.path());
	}

	//for (auto &filepath : sorted_by_name) {
	//	std::cout << filepath.c_str() << std::endl;
	//}
	int proj_count = 0;

	float * tmp_proj;
	for (auto &filepath : sorted_by_name) {

		DcmFileFormat file_format;
		OFCondition status = file_format.loadFile(filepath.c_str());
		if (status.bad()) {
			std::cerr << "Problem opening file:" << filepath << std::endl;
			exit(1);
		}

		DcmDataset * dataset = file_format.getDataset();

		// Minimal data to read for reconstruction is rows, channels, and projections

		// V1 issue: NO WAY to know if dataset is complete.  No total
		// number of projections in the header, thus, we must rely on
		// # of files, but there's no requirement that it's complete
		// (or way to check).  This has data integrity implications as well (no obvious
		// way to hash/checksum.
		Uint16 n_rows;
		Uint16 n_chan;
		Uint16 n_proj;
		if (!raw->IsInitialized()) {
			OFCondition condition;
			condition = dataset->findAndGetUint16(DCM_Rows, n_rows);
			if (condition.bad()) {
				std::cout << "ERROR reading rows" << std::endl;
				exit(1);
			}

			condition = dataset->findAndGetUint16(DCM_Columns, n_chan);
			if (condition.bad()) {
				std::cout << "ERROR reading rows" << std::endl;
				exit(1);
			}

			n_proj = sorted_by_name.size();
			//std::cout << "Got projections: " << n_proj << std::endl;

			tmp_proj = (float *)malloc(n_rows * n_chan * sizeof(float));

			raw->Allocate(n_rows, n_chan, n_proj);
		}

		// Read the pixeldata into our prellocated array
		Float64 rescale_slope;
		Float64 rescale_intercept;
		OFCondition cond;
		cond = dataset->findAndGetFloat64(DCM_RescaleSlope, rescale_slope);
		cond = dataset->findAndGetFloat64(DCM_RescaleIntercept, rescale_intercept);

		//std::cout << rescale_slope << std::endl;
		//std::cout << rescale_intercept << std::endl;

		// Get the raw pixel data
		const uint16_t * raw_pixel_data = NULL;
		unsigned long count;
		dataset->findAndGetUint16Array(DcmTagKey(0x7fe0,0x0010),raw_pixel_data,&count,false);
		uint16_t * tmp_ptr = (uint16_t *)raw_pixel_data;

		// TODO: Handle transpose cost
		for (int i = 0; i<n_rows*n_chan; i++) {
			tmp_proj[i] = rescale_slope*tmp_ptr[i] + rescale_intercept;
		}

		raw->SetProjection(proj_count, tmp_proj);

		// Transform it into float data
		proj_count++;
	}

	free(tmp_proj);
}

void ReadV2Dataset(std::string filepath, RawData * raw) {

	DcmFileFormat file_format;
	OFCondition status = file_format.loadFile(filepath.c_str());
	if (status.bad()) {
		std::cerr << "Problem opening file:" << filepath << std::endl;
		exit(1);
	}

	DcmDataset * dataset = file_format.getDataset();

	// Minimal data to read for reconstruction is rows, channels, and projections

	// V1 issue: NO WAY to know if dataset is complete.  No total
	// number of projections in the header, thus, we must rely on
	// # of files, but there's no requirement that it's complete
	// (or way to check).  This has data integrity implications as well (no obvious
	// way to hash/checksum.
	uint16_t n_rows;
	uint16_t n_chan;
	uint16_t m_detector_channels;
    uint16_t m_detector_rows;
	int n_proj;
	if (!raw->IsInitialized()) {

		status = dataset->findAndGetUint16(DcmTagKey(0x0028,0x0010),n_rows);
		status = dataset->findAndGetUint16(DcmTagKey(0x0028,0x0011),n_chan);
		status = dataset->findAndGetUint16(DcmTagKey(0x7029,0x1010),m_detector_rows);
		status = dataset->findAndGetUint16(DcmTagKey(0x7029,0x1011),m_detector_channels);
	}

	// Read the pixeldata into our prellocated array
	Float64 rescale_slope;
	Float64 rescale_intercept;
	OFCondition cond;
	cond = dataset->findAndGetFloat64(DCM_RescaleSlope, rescale_slope);
	cond = dataset->findAndGetFloat64(DCM_RescaleIntercept, rescale_intercept);

	//std::cout << rescale_slope << std::endl;
	//std::cout << rescale_intercept << std::endl;

	// Get the raw pixel data
	const uint16_t * raw_pixel_data = NULL;
	unsigned long count;
	dataset->findAndGetUint16Array(DcmTagKey(0x7fe0,0x0010),raw_pixel_data,&count,false);
	uint16_t * tmp_ptr = (uint16_t *)raw_pixel_data;

	// This line should ideally be up in raw->IsInitialized(), but there's bug in the FreeCT dicom writer
	// that is returning the incorrect number of Rows for the image data.  Will relocated once the bug is
	// fixed.
	n_proj = count/(m_detector_channels * m_detector_rows);
	raw->Allocate(m_detector_rows, m_detector_channels, n_proj);

	// TODO: Handle transpose cost
	float * tmp_proj = (float *)malloc(n_rows * n_chan * n_proj * sizeof(float));
	for (int i = 0; i<n_rows*n_chan*n_proj; i++) {
		tmp_proj[i] = rescale_slope*tmp_ptr[i] + rescale_intercept;
	}

	// MOST efficient way:
	//raw->Set(n_rows, n_chan, 1, tmp_proj); // Set is inherently more efficient than SetProjection b/c there is no memory copy

	// More akin to how ReadV1Dataset is doing it:
	// Read detector dimensions
	for (int i=0; i<n_proj; i++) {
		size_t offset = m_detector_rows * m_detector_channels * i;
		raw->SetProjection(i, &tmp_proj[offset]);
	}

	free(tmp_proj);

}