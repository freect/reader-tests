function dicom_stack = get_dicom_minimal(dicom_dir)
    
    file_list=dir(dicom_dir);
    file_list=file_list(~[file_list.isdir]);
    file_list={file_list.name};

    fullpath = strcat(dicom_dir,'/',file_list{1});

    h = dicominfo(fullpath);
    dicom_stack=zeros(h.Rows, h.Columns, numel(file_list));
    tmp_stack=cell(1,numel(file_list));
    
    for i=1:numel(file_list)
        dicom_stack(:,:,i)=dicomread(fullfile(dicom_dir,file_list{i}));
    end
    
    rescale_slope=h.RescaleSlope;
    rescale_intercept=h.RescaleIntercept;
    
    dicom_stack=rescale_slope*dicom_stack+rescale_intercept;
end

