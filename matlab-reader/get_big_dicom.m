function dicom_stack = get_big_dicom(filepath)
    h = dicominfo(filepath);
    v = dicomread(filepath);
    dicom_stack = squeeze(h.RescaleSlope*double(v) + h.RescaleIntercept);
end

