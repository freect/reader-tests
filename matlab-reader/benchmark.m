v1_cases = {
    "TCIA-LDCT-PD/C001",
    "TCIA-LDCT-PD/C002",
    "TCIA-LDCT-PD/C004",
    "TCIA-LDCT-PD/C009",
    "TCIA-LDCT-PD/C012",
    "TCIA-LDCT-PD/C016",
    "TCIA-LDCT-PD/C019",
    "TCIA-LDCT-PD/C021",
    "TCIA-LDCT-PD/C023",
    "TCIA-LDCT-PD/C027",
           };

v2_cases = {
    "TCIA-LDCT-PD-v2/C001.dcm",
    "TCIA-LDCT-PD-v2/C002.dcm",
    "TCIA-LDCT-PD-v2/C004.dcm",
    "TCIA-LDCT-PD-v2/C009.dcm",
    "TCIA-LDCT-PD-v2/C012.dcm",
    "TCIA-LDCT-PD-v2/C016.dcm",
    "TCIA-LDCT-PD-v2/C019.dcm",
    "TCIA-LDCT-PD-v2/C021.dcm",
    "TCIA-LDCT-PD-v2/C023.dcm",
    "TCIA-LDCT-PD-v2/C027.dcm",
           };

N = 4;


v2_results = cell(11,5);
v2_results(1,:) = {'patient', 'r1', 'r2', 'r3', 'r4'}
for i = 1:length(v2_cases)
   curr_case = v2_cases{i};
   fprintf(1, "Testing case: %s\n", curr_case);
   v2_results{i, 1} = curr_case;
   
   for iter = 1:N
        fprintf(1,"  read %d... ", iter);
        a = tic();
        img = get_big_dicom(curr_case);
        b = toc(a);
        fprintf(1, "%.2f s\n", b);
        v2_results{i, iter + 1} = b;
    end
   
end
writecell(v2_results, "results_v2.csv");

v1_results = cell(11,5);
v1_results(1,:) = {'patient', 'r1', 'r2', 'r3', 'r4'}
for i = 1:length(v1_cases)
    
    curr_case = v1_cases{i};
    
    fprintf(1, "Testing case: %s\n", curr_case);
    v1_results{i, 1} = curr_case;
    for iter = 1:N
        fprintf(1,"  read %d... ", iter);
        a = tic();
        img = get_dicom_minimal(curr_case);
        b = toc(a);
        fprintf(1, "%.2f s\n", b);
        v1_results{i, iter + 1} = b;
    end
end
writecell(v1_results, "results_v1.csv");