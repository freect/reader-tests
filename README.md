# FreeCT Reader Tests

This repo houses performance benchmarks for the single-file version of DICOM-CT-PD (a.k.a. SF-CT-PD... working title... likely to change).


## Contributing other readers

Don't see your favorite language or library?  Please feel free to submit it via an issue or merge request!

Some guidelines:
- Each folder in this repo should "stand alone" and not depend on any of the other readers.  
- External dependencies (e.g., DCMTK) are ok, however reasonable compilation instructions (or a script) should be included.  If it won't compile and run within 15 minutes, then it needs more work.

## Feedback

We are not generally planning to provide any ongoing routine maintenance to this repo since these readers are not consider "official" FreeCT readers.  However, if you find bugs or have optimizations to suggest, please use issues or a merge request (issues preferred.)

Optimizations will be considered, however the intent of this work is to provide results similar to what an "average" researcher might write on their own, not to precisely and perfectly optimize any one reader.  Please keep that in mind.

## Dedicated Reader Repository

If there is enough interest in having the FreeCT Project provide "official" readers for the DICOM-CT-PD and SF-CT-PD in different languages, please let us know by starring this repo and opening up an issue (or upvoting an existing issue).  The feedback can help us gauge whether or not that would be a worthwhile effort.

## License

This code is covered by the same license as the full FreeCT project (effectively BSD 3-clause with a "citation" agreement).  In other words, **for academic, personal, or exploratory work** you are free to use, repurpose, rewrite, anything in this repo with the requirement that the relevant FreeCT work is cited and retains the original license agreement.

Please note that the the FreeCT license does not currently cover use of FreeCT in industry.  If you are interested in using it for industry research or deployment, please contact John Hoffman (johnmarianhoffman@gmail.com) directly for further discussion.
