import pydicom as dicom
import os, time, sys

def ReadFile(fp: str):
    start = time.time()
    dicom.dcmread(fp)
    return time.time() - start

def ReadDataSet(fp: str):
    files = os.listdir(fp)
    if fp[-1] != '/':
        fp += '/'
    start = time.time()
    for f in files:
        dicom.dcmread(fp+f)
    return time.time() - start

N = 10
print("V2 results:")
for i in range(N):
    print("{:2.8} seconds".format(ReadFile(sys.argv[1])))

print("V1 results:")
for i in range(N):
    print("{:2.8} seconds".format(ReadDataSet(sys.argv[2])))