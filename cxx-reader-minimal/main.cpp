// NOTE: Although this is a minimal example of allocating required
// memory and reading files, it should NOT be considered reflective of
// actual expected reader performance due to compiler optimizations,
// kernel differences, etc.  Please instead use the cxx-reader-dcmtk
// for a more "honest" evaluation.

#include <iostream>
#include <fstream>
#include <filesystem>
#include <string>
#include <vector>
#include <cstdlib>

class Timer {
public:
    std::chrono::time_point<std::chrono::system_clock> start, end;

	Timer(){}
	~Timer(){}

	void Start() {
		start = std::chrono::system_clock::now();
	}

	void Stop() {
		end = std::chrono::system_clock::now();
		std::chrono::duration<double> elapsed = end-start;
		std::cout << elapsed.count() << " seconds" << std::endl;
	}
};
int count = 0;
// Read data into memory from disk
size_t ReadFile(std::string s) {
	size_t bytes_read = 0;

	std::filesystem::path p{s};
	size_t file_size = std::filesystem::file_size(p);
	char * data = (char *)malloc(file_size);
	std::ifstream input_file(p);
	input_file.read(data, file_size);
	if (data[rand()%(file_size-20)]==0x1) {
		count++;
	}
	free(data);
	return file_size;
}

size_t ReadDataSet(std::string s) {
	size_t bytes_read = 0;

	std::filesystem::path p{s};
	for (const auto & entry : std::filesystem::directory_iterator(p)) {
		bytes_read += ReadFile(entry.path());
	}

	return bytes_read;
}

int main(int argc, char ** argv) {

	int N=4;

	std::vector<double> v1_read_times(N);
	std::vector<double> v2_read_times(N);

	Timer t  = Timer();

	std::cout << "Reading v2" << std::endl;
	for (int i=0;i<N;i++) {
		t.Start();
		int n_read = ReadFile(std::string(argv[1]));
		std::cout << "MBytes_read: "<< n_read/1000000 << std::endl;
		t.Stop();
	}

	std::cout << count << std::endl;

	std::cout << "Reading v1" << std::endl;
	for  (int i=0;i<N; i++){
		t.Start();
		int n_read =  ReadDataSet(std::string(argv[2]));
		std::cout << "MBytes_read: "<< n_read/1000000 << std::endl;
		t.Stop();
	}

	std::cout << count << std::endl;

	return 0;
}